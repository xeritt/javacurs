package ru.eltex.app.lab1;


import ru.eltex.app.js.Test;

public class LightAuto extends Car {

    String model;

    public LightAuto(){
        super();
        System.out.println(Test.LIGHT_CAR);
    }

    @Override
    public void create() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}
