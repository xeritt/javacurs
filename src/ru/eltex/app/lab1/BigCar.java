package ru.eltex.app.lab1;

import ru.eltex.app.js.Test;

import java.sql.SQLOutput;

public class BigCar extends Car {
    String kuzov;

    public BigCar(){
        super();
        System.out.println(Test.BIG_CAR);
    }

    public BigCar(String message){
        this();
        System.out.println(message);
    }

    @Override
    public void create() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}
