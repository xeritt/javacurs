package ru.eltex.app.lab1;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.UUID;

public abstract class Car implements ICrudAction {
    static int count = 0;
    private UUID ID;
    String makr;

    Car(){
        ID = UUID.randomUUID();
        count++;
    }

    @Override
    public void read() {
        System.out.println(ID);
        System.out.println(count);
    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
    }

    public UUID getUUID(){
        return ID;
    };
}
