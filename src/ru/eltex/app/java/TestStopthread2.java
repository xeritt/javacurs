package ru.eltex.app.java;

public class TestStopthread2 extends Thread {

    @Override
    public void run() {
        while (!isInterrupted()){
            System.out.println("Hello");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }

    public static void main(String[] args) {
        TestStopthread2 th = new TestStopthread2();
        th.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        th.interrupt();
    }
}
