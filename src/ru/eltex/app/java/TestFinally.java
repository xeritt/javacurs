package ru.eltex.app.java;

public class TestFinally{
    public int test(){
        try {
            throw new Exception();
        } catch (Exception npe){
            System.out.println("Exception");
            return 0;
            //System.exit(0);
        } finally{
            System.out.println("Finally");
        }

        //return 1;
    }

    static public void main(String args[]){
        System.out.println("Hello");
        TestFinally main = new TestFinally();
        int i = main.test();
    }
}