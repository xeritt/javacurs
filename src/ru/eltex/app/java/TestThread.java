package ru.eltex.app.java;

public class TestThread  implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i <10; i++) {
            System.out.println("i="+i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {

        Thread th = new Thread(new TestThread());
        Thread th2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <10; i++) {
                    System.out.println("j="+i);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        th.start();
        th2.start();
    }

}
