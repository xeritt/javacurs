package ru.eltex.app.java;

public class Eclipse extends Point {
    private int r1;
    private int r2;

    public int getR1() {
        return r1;
    }

    public void setR1(int r1) {
        this.r1 = r1;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }

    public void show(){
        super.show();
        System.out.println("r1="+r1);
        System.out.println("r2="+r2);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean up(int dh) {
        y += dh;
        return false;
    }

    @Override
    public boolean down(int dh) {
        return false;
    }
}
