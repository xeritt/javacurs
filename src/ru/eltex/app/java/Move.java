package ru.eltex.app.java;

public interface Move {
    public boolean up(int dh);
    public boolean down(int dh);

    default void showEx1(){
        System.out.println("Hello");
    }
}
