package ru.eltex.app.java;


class Data{ public String name;}
class Sync implements Runnable{
    private Data data;
    Sync(Data dt){data = dt;}
    public void run() {
        synchronized(data){
            data.name = Thread.currentThread().getName();
            print("before: " + data.name);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException x) {
            }
            print("after: " + data.name);
        }
    }

    public static void print(String msg) {
        System.out.println(Thread.currentThread().getName() + ": " + msg);
    }
}

public class TestSynchronized{
    public static void main(String[] args) throws Exception{
        Data id = new Data();
        Sync runA = new Sync(id);
        Sync runB = new Sync(id);
        Sync runC = new Sync(id);
        Thread ta = new Thread(runA, "threadA");
        Thread tb = new Thread(runB, "threadB");
        Thread tc = new Thread(runC, "threadC");
        ta.start();
        tb.start();
        tc.start();
    }
}


