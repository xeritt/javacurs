package ru.eltex.app.java;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Item{
    public String name;
    Item(String name){this.name = name;}
}


public class TestRemove {

    public static void main(String[] args) {
        List<Item> list = new ArrayList<>();
        list.add(new Item("Hello0"));
        list.add(new Item("Hello1"));
        list.add(new Item("Hello2"));

        int i =0;
        for (Item s:list) {
         //   list.remove(s); //erro
            System.out.println(s.name);
            s.name += "!";
            System.out.println(list.get(i).name);
            i++;
        }
        Iterator<Item> iterator = list.iterator();
        for (;iterator.hasNext();){
            Item itm = iterator.next();
            System.out.println(itm.name);
            iterator.remove();
        }
        System.out.println("----------------------");
        for (Item s:list) {
            // list.remove(s);
            System.out.println(s.name);
        }

    }
}
