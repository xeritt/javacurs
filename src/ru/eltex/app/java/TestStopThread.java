package ru.eltex.app.java;

public class TestStopThread implements Runnable{

    public boolean fRun = true;

    @Override
    public void run() {
        while (fRun){
            System.out.println("Hello");
            try {Thread.sleep(1000);
            } catch (InterruptedException e) {e.printStackTrace();}
        }
    }

    public static void main(String[] args) {
        TestStopThread test = new TestStopThread();
        Thread th = new Thread(test);
        th.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        test.fRun = false;

    }

}
