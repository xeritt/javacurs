package ru.eltex.app.java;

class Sync3 implements Runnable{
    private Thread monitor;
    public  Sync3 setMonitor(Thread mon){
        monitor = mon;
        return this;
    }
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + ": start");
            if (monitor == null)
                Thread.sleep(1000);
            else
                monitor.join(); //ожидание выполнения потока monitor
            System.out.println(Thread.currentThread().getName() + ": end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
public class TestJoinMethod{
    public static void main(String[] args) throws Exception{
        Thread ta = new Thread(new Sync3(), "threadA");
        Thread tb = new Thread(new Sync3().setMonitor(ta), "threadB");
        ta.start();
        tb.start();
        try {
            tb.join(); //ожидание выполнения потока tb
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ": end");
    }
}