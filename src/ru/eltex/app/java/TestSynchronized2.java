package ru.eltex.app.java;

class Sync2 implements Runnable{
    private String data;
    Sync2(String dt){data = dt;}
    public void run() {
        //synchronized(data){
            data = Thread.currentThread().getName();
            print("before: " + data);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException x) {
            }
            print("after: " + data);
       // }
    }

    public static void print(String msg) {
        System.out.println(Thread.currentThread().getName() + ": " + msg);
    }
}

public class TestSynchronized2{
    public static void main(String[] args) throws Exception{
        String id = "Hello";
        Sync2 runA = new Sync2(id);
        Sync2 runB = new Sync2(id);
        Sync2 runC = new Sync2(id);
        Thread ta = new Thread(runA, "threadA");
        Thread tb = new Thread(runB, "threadB");
        Thread tc = new Thread(runC, "threadC");
        ta.start();
        tb.start();
        tc.start();
    }
}


