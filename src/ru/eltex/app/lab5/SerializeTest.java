package ru.eltex.app.lab5;
import java.io.*;

class Person implements Serializable {
    public String name = null;
    public int age = 0;
}

public class SerializeTest {

    public static final String PERSON_file = "person.bin";

    public void save(){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(PERSON_file))) {
            Person person = new Person();
            person.name = "Jakob Jenkov";
            person.age = 40;
            objectOutputStream.writeObject(person);
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(){
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.bin"))){
                Person personRead = (Person) objectInputStream.readObject();
                System.out.println(personRead.name);
                System.out.println(personRead.age);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SerializeTest test = new SerializeTest();
        test.save();
        test.load();
    }
}
