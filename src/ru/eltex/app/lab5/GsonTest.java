package ru.eltex.app.lab5;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GsonTest {
    public static final String FILE_JSON = "staff.json";

    private static Staff createStaffObject() {

        Staff staff = new Staff();

        staff.setName("mkyong");
        staff.setAge(35);
        staff.setPosition(new String[]{"Founder", "CTO", "Writer"});
        Map<String, BigDecimal> salary = new HashMap() {{
            put("2010", new BigDecimal(10000));
            put("2012", new BigDecimal(12000));
            put("2018", new BigDecimal(14000));
        }};
        staff.setSalary(salary);
        staff.setSkills(Arrays.asList("java", "python", "node", "kotlin"));

        return staff;

    }

    public void save(){
        //Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Staff staff = createStaffObject();
        // Java objects to File
        try (FileWriter writer = new FileWriter(FILE_JSON)) {
            gson.toJson(staff, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(){
        Gson gson = new Gson();

        try (Reader reader = new FileReader(FILE_JSON)) {
            // Convert JSON File to Java Object
            Staff staff = gson.fromJson(reader, Staff.class);
            // print staff object
            //System.out.println(staff);
            System.out.println(staff.getName());
            System.out.println(staff.getAge());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello");
        GsonTest main = new GsonTest();
        main.save();
        main.load();
    }
}
