package ru.eltex.app.lab5;

import java.io.*;

public class InputStreamReaderTest {
    public static void main(String[] args) {
        try (Reader reader = new InputStreamReader(
                new FileInputStream("notes.txt"), "Cp1251");
             Writer writer = new OutputStreamWriter(
                new FileOutputStream("output.txt"), "Cp866") ) {

                int c = 0;
                while ((c = reader.read()) >= 0) writer.write(c);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
