package ru.eltex.app.lab5;

import java.io.*;

public class ReaderWriterTest {
    public static void main(String[] args) {

        try (Reader reader = new FileReader("notes.txt")){
            try (Writer writer = new FileWriter("output.txt")) {
                int c = 0;
                while ((c = reader.read()) >= 0) {
                    writer.write(Character.toUpperCase((char) c));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
