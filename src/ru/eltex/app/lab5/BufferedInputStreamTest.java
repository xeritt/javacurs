package ru.eltex.app.lab5;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

public class BufferedInputStreamTest {
    public static void main(String[] args) {
        try(BufferedInputStream bis = new BufferedInputStream(
                new FileInputStream(new File("/etc/hosts")))) {

                int c;
                while ((c = bis.read()) != -1) {
                    System.out.print((char) c);
                }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
