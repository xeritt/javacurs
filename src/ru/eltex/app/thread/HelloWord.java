package ru.eltex.app.thread;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class HelloWord {
    public static void main(String []args) throws InterruptedException {
        Lock lock = new ReentrantLock();
        Runnable task = new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println("Thread");
                lock.unlock();
            }
        };
        lock.lock();

        Thread th = new Thread(task);
        th.start();
        System.out.println("main");
        Thread.currentThread().sleep(25000);
        lock.unlock();
        Thread.currentThread().sleep(25000);
    }
}
