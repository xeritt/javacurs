package ru.eltex.app.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.function.Supplier;

public class FutureTaskTest {

    public static void main(String[] args) throws Exception {
        Callable task = () -> {
            return "Hello, World!";
        };
        FutureTask<String> future = new FutureTask<String>(task);
        new Thread(future).start();
        System.out.println(future.get());
        Supplier<String> supplier = () -> "String";
    }

}
