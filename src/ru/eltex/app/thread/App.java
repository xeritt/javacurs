package ru.eltex.app.thread;

public class App {
    volatile public boolean flag = true;

    public void go()  throws InterruptedException {
        Runnable whileFlagFalse = new Runnable() {
            @Override
            public void run() {
                while (flag) {
                    //Thread.sleep(1000);
                   // System.out.println("While");
                }
                System.out.println("Flag is now TRUE");
            }
        };

        new Thread(whileFlagFalse).start();
        Thread.sleep(1000);
        System.out.println("Hello");

    }

    public static void main(String[] args)  throws InterruptedException {
        App ap = new App();
        ap.go();
        System.out.println("done");
        ap.flag = false;
    }
}
