package ru.eltex.app.lab2;

import java.sql.Date;

public class Order {

    private ShoppingCart cart;
    private Credentials user;
    private OrderStatus status;
    private Date dateCreate;
    private long timeWaiting; //сколько времени в милсек. ждет заказ

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public long getTimeWaiting() {
        return timeWaiting;
    }

    public long getInterval() {
        return dateCreate.getTime() + timeWaiting;
    }

    public boolean checkInterval(long time) {
        if ((dateCreate.getTime() + timeWaiting) < time) {
            return true;
        } else
            return false;
    }

    public Order(ShoppingCart cart, Credentials user) {
        this.cart = cart;
        this.user = user;
        status = OrderStatus.WAITING;
        dateCreate = new Date(System.currentTimeMillis());
        //timeWaiting = (int)(Math.random() * 1000);
        timeWaiting = 1;
    }


    public void show(){
        cart.show();
        user.show();

        //language=RegExp
        String regex = "[A-Za-z ]*";

        System.out.println("status:"+status);
        System.out.println("date create:"+dateCreate);
        System.out.println("time:"+timeWaiting);
    }
}
