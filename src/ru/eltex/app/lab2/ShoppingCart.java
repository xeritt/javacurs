package ru.eltex.app.lab2;

import ru.eltex.app.lab1.Car;

import java.util.*;

public class ShoppingCart {

    private List<Car> products;
    private Set<UUID> uuids;

    public ShoppingCart() {
        this.products = new ArrayList<>();
        this.uuids = new HashSet<>();
    }

    /**
     * корзина
     * @param products список продуктов
     */
    public ShoppingCart(List<Car> products) {
        this.products = products;
    }

    public boolean add(Car car){
        uuids.add(car.getUUID());
        return products.add(car);
    }

    public void show(){
        for (Car car: products) {
            car.read();
        }
    }

    public boolean isExistsUUID(UUID id){
        return uuids.contains(id);
    }

}
