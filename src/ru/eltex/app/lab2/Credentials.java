package ru.eltex.app.lab2;

import java.util.UUID;

public class Credentials {

    private UUID ID;
    private String firstName;
    private String secondName;
    private String email;

    public Credentials(String firstName, String secondName, String email) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.ID = UUID.randomUUID();
    }

    public void show(){
        System.out.println("ID="+ID);
        System.out.println("firstName="+firstName);
        System.out.println("secondName="+secondName);
        System.out.println("email="+email);
    }
}
