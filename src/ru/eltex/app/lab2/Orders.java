package ru.eltex.app.lab2;

import javax.swing.text.html.HTMLDocument;
import java.util.*;

public class Orders {

    private List<Order> orders;
    private Map<Date, Order> dateOrder;

    public Orders() {
        this.orders = new LinkedList<>();
        this.dateOrder = new HashMap<>();
    }

    public Orders(List<Order> orders) {
        this.orders = orders;
    }

    public void offer(ShoppingCart cart, Credentials user) {
        Order order = new Order(cart, user);
        orders.add(order);
        dateOrder.put(order.getDateCreate(), order);
    }

    public void checkTime() {
        synchronized(orders) {
            Iterator it = orders.iterator();
            while (it.hasNext()) {
                Order order = (Order) it.next();
                if (order.getStatus() == OrderStatus.WAITING &&
                        order.checkInterval(System.currentTimeMillis())) {
                    order.setStatus(OrderStatus.DONE);
                    System.out.printf("Check time");
                }
            }
        }
    }

    public void checkDone() {

        synchronized (orders) {
            Iterator it = orders.iterator();
            while (it.hasNext()) {
                Order order = (Order) it.next();
                if (order.getStatus() != OrderStatus.DONE) {
                    //orders.remove(order);//зарыта соббака
                    it.remove();
                    System.out.printf("Delete order");
                }
            }
        }
    }

    public void show() {
        for (Order order : orders) {
            System.out.println("--------------------------");
            order.show();

        }

    }


}
