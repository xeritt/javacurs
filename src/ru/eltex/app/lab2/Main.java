package ru.eltex.app.lab2;

import ru.eltex.app.lab1.BigCar;
import ru.eltex.app.lab1.Car;
import ru.eltex.app.lab1.LightAuto;

import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Credentials user = new Credentials("Nick", "Chi", "tut@g,ail.com");

        ShoppingCart cart = new ShoppingCart();
        cart.add(new BigCar());
        cart.add(new LightAuto());
        Orders orders = new Orders();
        orders.offer(cart, user);

//        ShoppingCart cart1 = new ShoppingCart();
//        cart1.add(new BigCar());
//        cart1.add(new LightAuto());
        Car car = new LightAuto();
        cart.add(car);
        orders.offer(cart, user);

        //orders.show();
        //orders.checkTime();
        //orders.show();

        System.out.println(cart.isExistsUUID(UUID.randomUUID()));
    }
}
