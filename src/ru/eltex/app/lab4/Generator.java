package ru.eltex.app.lab4;

import ru.eltex.app.lab1.BigCar;
import ru.eltex.app.lab1.Car;
import ru.eltex.app.lab1.LightAuto;
import ru.eltex.app.lab2.Credentials;
import ru.eltex.app.lab2.Orders;
import ru.eltex.app.lab2.ShoppingCart;

public class Generator extends ACheck{


    Generator(Orders orders) {
        super(orders);
    }

    @Override
    public void run() {
        while(fRun){
            Orders orders = getOrders();
            ShoppingCart cart = new ShoppingCart();
            for (int j = 0; j<3; j++){
                Car car1 = new LightAuto();
                car1.create();
                cart.add(car1);
                Car car2 = new BigCar();
                car2.create();
                cart.add(car2);
            }

            Credentials user = new Credentials("Nick", "chi", "test@gmail.com");
            synchronized (orders){
                orders.offer(cart, user);
            }
            try {
                Thread.sleep(pause);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
