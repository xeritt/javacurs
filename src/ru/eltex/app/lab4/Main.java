package ru.eltex.app.lab4;

import ru.eltex.app.lab2.Orders;

public class Main {

    public static void main(String[] args) {

        Orders orders = new Orders();

        Thread gen1 = new Thread(new Generator(orders));
        Thread gen2 = new Thread(new Generator(orders));
        gen1.start();
        gen2.start();

        Thread checkTime = new Thread(new CheckTime(orders, 1000));
        Thread checkDone = new Thread(new CheckDone(orders, 1000));
        checkTime.start();
        checkDone.start();
    }
}
