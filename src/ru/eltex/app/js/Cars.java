package ru.eltex.app.js;

public enum Cars {
    LIGHTCAR,
    BIGCAR,
    NONE;

    public void info(){
        System.out.printf(String.valueOf(this));
    }
}
